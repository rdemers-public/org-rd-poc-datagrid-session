package org.demers.poc.datagrid.session.service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.demers.poc.datagrid.session.domain.NoSeriableClass;
import org.demers.poc.datagrid.session.domain.User;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

// Il est important d'activer le "proxy" et la session.
@Component
@Scope(proxyMode=ScopedProxyMode.TARGET_CLASS, value=WebApplicationContext.SCOPE_SESSION)
// ou @SessionScope
public class UserRepository implements Serializable 
{
    private static final long serialVersionUID = 6836928118275060841L;

    // Les classes qui ne sont pas Serializable doivent être transient.
    private transient NoSeriableClass noSeriableClass;

    private Map<Integer,User> userMap;
    private Random rand;

    public UserRepository()
    {
        super();
        rand    = new Random();
        userMap = new ConcurrentHashMap<>();

        noSeriableClass = new NoSeriableClass(100); // Pour les tests.
    }

    public void add(User user)
    {
        Integer id;

        do id = rand.nextInt(999999);
        while (userMap.containsKey(id));

        user.setId(id);
        userMap.put(id, user);
    }

    public List<User> findAll()
    {
        return new ArrayList<User>(userMap.values());
    }
}