package org.demers.poc.datagrid.session.domain;

public class NoSeriableClass
{
    private int id;

    public NoSeriableClass(int id)
    {
        this.id = id;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }
}