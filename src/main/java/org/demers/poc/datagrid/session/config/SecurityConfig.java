package org.demers.poc.datagrid.session.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
	@Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception	
    {
        auth.inMemoryAuthentication().withUser(User.withUsername("demers").password("{noop}demers").roles("USER").build());
    }
}