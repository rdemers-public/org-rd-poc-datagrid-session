package org.demers.poc.datagrid.session.controller;

import org.demers.poc.datagrid.session.domain.User;
import org.demers.poc.datagrid.session.service.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class UserController
{
    @Autowired
    UserRepository userRepository;

    @GetMapping("/users/add")
    public String userAdd(User user)
    {
        return "users-add";
    }

    @GetMapping("/users/list")
    public String usersList(Model model)
    {
        model.addAttribute("users", userRepository.findAll());
        return "users-list";
    }

    @PostMapping("/action/addUser")
    public String addUser(User user, Model model)
    {
        userRepository.add(user);
        model.addAttribute("users", userRepository.findAll());
        return "redirect:/users/list";
    }
}