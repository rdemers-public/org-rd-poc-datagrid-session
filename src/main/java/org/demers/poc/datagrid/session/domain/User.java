package org.demers.poc.datagrid.session.domain;

import java.io.Serializable;

public class User implements Serializable
{
    private static final long serialVersionUID = 5892469241322441165L;

    private Integer id;
    private String  title;
    private String  shortName;
    private String  familyName;

    public User(String title, String shortName, String familyName)
    {
        this.id         = null;
        this.title      = title;
        this.shortName  = shortName;
        this.familyName = familyName;
    }

    public Integer getId()
    {
        return id;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public String getTitle()
    {
        return title;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String getShortName()
    {
        return shortName;
    }
	
    public void setShortName(String shortName)
    {
        this.shortName = shortName;
    }

    public String getFamilyName()
    {
        return familyName;
    }

    public void setFamilyName(String familyName)
    {
        this.familyName = familyName;
    }
}