package org.demers.poc.datagrid.session.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
    ({ 
    "org.demers.poc.datagrid.session.domain",
    "org.demers.poc.datagrid.session.service",
    "org.demers.poc.datagrid.session.controller"
    })
public class ApplicationConfig
{
    public ApplicationConfig()
    {
        super();
    }
}