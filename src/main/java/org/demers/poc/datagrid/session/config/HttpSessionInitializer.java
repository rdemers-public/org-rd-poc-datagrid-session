package org.demers.poc.datagrid.session.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

// S'assure simpplement que le filtre pour la gestion de la session passe "avant" le filtre de la sécurité.
@Configuration
public class HttpSessionInitializer extends AbstractHttpSessionApplicationInitializer
{
    public HttpSessionInitializer()
    {
        super();
    }
}